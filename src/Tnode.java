import java.lang.reflect.Array;
import java.util.*;

public class Tnode {

    private String name;
    private Tnode firstChild;
    private Tnode nextSibling;
    private static final String splitter = " ";


    public Tnode(String name) {
        this.name = name;
    }


    public Tnode(String name, Tnode firstChild, Tnode nextSibling) {
        this.name = name;
        this.firstChild = firstChild;
        this.nextSibling = nextSibling;
    }


    @Override
    public String toString() {
        StringBuffer b = new StringBuffer();
        Tnode tnode = firstChild;
        b.append(name);
        if (tnode != null) {
            b.append('(');
        }
        while (tnode != null) {
            b.append(tnode.toString());
            if (tnode.nextSibling != null) {
                b.append(',');
            } else {
                b.append(')');
            }
            tnode = tnode.nextSibling;
        }
        return b.toString();
    }


    private static String[] strToArray(String str) {
        return str.split(splitter);
    }

    private static boolean isCorrectSymbol(String symbol) {
        if (!symbol.matches("((-?([0-9]{1,}))|[-+*/])")) {
            return false;
        } else {
            return true;
        }

    }

    private boolean isNumeric() {
        try {
            Integer.parseInt(this.name);
        } catch (NumberFormatException ex) {
            return false;
        }
        return true;
    }

    public static Tnode buildFromRPN(String pol) {
        Tnode root = null;
        Tnode first, second;
        Stack<Tnode> stack = new Stack<Tnode>();
        if (pol.isEmpty()) {
            throw new RuntimeException("RPN string is empty");
        }
        for (String str : strToArray(pol)) {
            if (!isCorrectSymbol(str)) {
                throw new RuntimeException("RPN <" + pol + "> contains illegal symbol <" + str + ">");
            }
            root = new Tnode(str);
            if (!root.isNumeric()) {
                if (stack.size() < 2) {
                    throw new RuntimeException("RPN <" + pol + "> contains to few numbers");
                }
                second = stack.pop();
                first = stack.pop();
                first.nextSibling = second;
                root.firstChild = first;
            }
            stack.push(root);
        }
        if (stack.size() > 1) {
            throw new RuntimeException("RPN <" + pol + "> contains to many numbers");
        }
        return root;
    }

    public static void main(String[] param) {
        // String rpn = "1 2 +";

        //String rpn = "2 1 - 4 * 6 3 / +";
        String rpn = "2 3 + 5";
        System.out.println("RPN: " + rpn);
        Tnode res = buildFromRPN(rpn);
        // System.out.println(readPolToList(rpn));
        // System.out.println(arrayToString(strToArray(rpn)));
        Tnode l2 = new Tnode("4");
        Tnode l1 = new Tnode("-", new Tnode("2", null, new Tnode("1", null, null)), l2);
        Tnode l4 = new Tnode("/", new Tnode("6", null, new Tnode("3", null, null)), null);
        Tnode l3 = new Tnode("*", l1, l4);
        Tnode l5 = new Tnode("+", l3, null);
        // res = l5;
        // res = new Tnode("+", new Tnode("1", null, new Tnode("2", null, null)), null);
        System.out.println("Need: +(*(-(2,1),4),/(6,3))");
        System.out.println("Tree: " + res);
        // TODO!!! Your tests here
    }
}

